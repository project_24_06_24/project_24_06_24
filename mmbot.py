import requests as rq
import random
DEBUG = False

class wordleBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(wordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(wordleBot.creat_url, json=creat_dict)

        self.choices = [w for w in wordleBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(wordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj.get("feedback", "")
            right = str(rj["feedback"])
            status = "GGGGG" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        while not won and len(tries) < 6:
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            if not self.choices:
                break
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        print("Word is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))
    def update(self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            for i in range(len(feedback)):
                if feedback[i] == 'G' and word[i] != choice[i]:
                    return False
                if feedback[i] == 'Y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                if feedback[i] == 'B' and choice[i] in word:
                    return False
            return True
        self.choices = [w for w in self.choices if matches_feedback(w, choice , feedback)]


game = wordleBot("CodeShifu")
game.play()

